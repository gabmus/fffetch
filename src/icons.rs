use std::fmt::Display;

#[derive(Debug, Clone, Copy)]
pub enum Icon {
    Cpu,
    Gpu,
    Tux,
    Memory,
    Motherboard,
    Firmware,
    Desktop,
    Arch,
    Debian,
    Fedora,
    Gentoo,
    Nix,
    Rhel,
    Suse,
    Ubuntu,
}

impl Display for Icon {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_str(match self {
            Self::Cpu => "󰍛",
            Self::Gpu => "\u{f03e}",
            Self::Tux => "\u{f31a}",
            Self::Memory => "\u{f1c0}",
            Self::Motherboard => "\u{eabe}",
            Self::Firmware => "\u{e266}",
            Self::Desktop => "\u{f108}",
            Self::Arch => "\u{f303}",
            Self::Debian => "\u{f306}",
            Self::Fedora => "\u{f30a}",
            Self::Gentoo => "\u{f30d}",
            Self::Nix => "\u{f313}",
            Self::Rhel => "󱄛",
            Self::Suse => "\u{f314}",
            Self::Ubuntu => "\u{f31b}",
        })
    }
}

impl Icon {
    pub fn for_distro(distro: &str) -> Self {
        match distro.to_lowercase() {
            d if d.contains("arch") => Self::Arch,
            d if d.contains("debian") => Self::Debian,
            d if d.contains("fedora") => Self::Fedora,
            d if d.contains("gentoo") => Self::Gentoo,
            d if d.contains("nix") => Self::Nix,
            d if d.contains("red hat") || d.contains("redhat") || d.contains("rhel") => Self::Rhel,
            d if d.contains("suse") => Self::Suse,
            d if d.contains("ubuntu") => Self::Ubuntu,
            _ => Self::Tux,
        }
    }
}
