use clap::Parser;

#[derive(Parser, Debug)]
#[command(author, version, about, long_about = None)]
pub struct Args {
    /// Show an ASCII frame around the data
    #[arg(long, default_value_t = false)]
    pub no_frame: bool,
    /// Show grid of colors
    #[arg(long, default_value_t = false)]
    pub colorgrid: bool,
}
