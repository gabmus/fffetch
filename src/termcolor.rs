const ANSI_RESET: &str = "\u{001B}[0m";

const ANSI_BLACK: &str = "\u{001B}[30m";
const ANSI_RED: &str = "\u{001B}[31m";
const ANSI_GREEN: &str = "\u{001B}[32m";
const ANSI_YELLOW: &str = "\u{001B}[33m";
const ANSI_BLUE: &str = "\u{001B}[34m";
const ANSI_PURPLE: &str = "\u{001B}[35m";
const ANSI_CYAN: &str = "\u{001B}[36m";
const ANSI_WHITE: &str = "\u{001B}[37m";
const ANSI_BOLD: &str = "\u{001B}[1m";

const ANSI_DARK_GRAY: &str = "\u{001B}[1;30m";
const ANSI_LIGHT_RED: &str = "\u{001B}[1;31m";
const ANSI_LIGHT_GREEN: &str = "\u{001B}[1;32m";
const ANSI_LIGHT_YELLOW: &str = "\u{001B}[1;33m";
const ANSI_LIGHT_BLUE: &str = "\u{001B}[1;34m";
const ANSI_LIGHT_PURPLE: &str = "\u{001B}[1;35m";
const ANSI_LIGHT_CYAN: &str = "\u{001B}[1;36m";
const ANSI_LIGHT_WHITE: &str = "\u{001B}[1;37m";

pub fn strip_color(s: &str) -> String {
    let mut res = s.to_string();
    [
        ANSI_RESET,
        ANSI_BLACK,
        ANSI_RED,
        ANSI_GREEN,
        ANSI_YELLOW,
        ANSI_BLUE,
        ANSI_PURPLE,
        ANSI_CYAN,
        ANSI_WHITE,
        ANSI_BOLD,
        ANSI_DARK_GRAY,
        ANSI_LIGHT_RED,
        ANSI_LIGHT_GREEN,
        ANSI_LIGHT_YELLOW,
        ANSI_LIGHT_BLUE,
        ANSI_LIGHT_PURPLE,
        ANSI_LIGHT_CYAN,
        ANSI_LIGHT_WHITE,
    ]
    .iter()
    .for_each(|style| res = res.replace(style, ""));
    res
}

fn t_style(txt: &str, style: &str) -> String {
    format!("{}{}{}", style, txt, ANSI_RESET)
}

pub fn t_black(txt: &str) -> String {
    t_style(txt, ANSI_BLACK)
}

pub fn t_red(txt: &str) -> String {
    t_style(txt, ANSI_RED)
}

pub fn t_green(txt: &str) -> String {
    t_style(txt, ANSI_GREEN)
}

pub fn t_yellow(txt: &str) -> String {
    t_style(txt, ANSI_YELLOW)
}

pub fn t_blue(txt: &str) -> String {
    t_style(txt, ANSI_BLUE)
}

pub fn t_purple(txt: &str) -> String {
    t_style(txt, ANSI_PURPLE)
}

pub fn t_cyan(txt: &str) -> String {
    t_style(txt, ANSI_CYAN)
}

pub fn t_white(txt: &str) -> String {
    t_style(txt, ANSI_WHITE)
}

pub fn t_bold(txt: &str) -> String {
    t_style(txt, ANSI_BOLD)
}

pub fn t_dark_gray(txt: &str) -> String {
    t_style(txt, ANSI_DARK_GRAY)
}

pub fn t_light_red(txt: &str) -> String {
    t_style(txt, ANSI_LIGHT_RED)
}

pub fn t_light_green(txt: &str) -> String {
    t_style(txt, ANSI_LIGHT_GREEN)
}

pub fn t_light_yellow(txt: &str) -> String {
    t_style(txt, ANSI_LIGHT_YELLOW)
}

pub fn t_light_blue(txt: &str) -> String {
    t_style(txt, ANSI_LIGHT_BLUE)
}

pub fn t_light_purple(txt: &str) -> String {
    t_style(txt, ANSI_LIGHT_PURPLE)
}

pub fn t_light_cyan(txt: &str) -> String {
    t_style(txt, ANSI_LIGHT_CYAN)
}

pub fn t_light_white(txt: &str) -> String {
    t_style(txt, ANSI_LIGHT_WHITE)
}

const BLOCKS: &str = "\u{2588}\u{2588}\u{2588}";

pub fn color_grid() -> String {
    format!(
        " {}{}{}{}{}{}{}{}\n {}{}{}{}{}{}{}{}",
        t_black(BLOCKS),
        t_red(BLOCKS),
        t_green(BLOCKS),
        t_yellow(BLOCKS),
        t_blue(BLOCKS),
        t_purple(BLOCKS),
        t_cyan(BLOCKS),
        t_white(BLOCKS),
        t_dark_gray(BLOCKS),
        t_light_red(BLOCKS),
        t_light_green(BLOCKS),
        t_light_yellow(BLOCKS),
        t_light_blue(BLOCKS),
        t_light_purple(BLOCKS),
        t_light_cyan(BLOCKS),
        t_light_white(BLOCKS),
    )
}
