use super::UNKNOWN;
use std::env;

pub fn desktop() -> String {
    env::var("XDG_CURRENT_DESKTOP").unwrap_or(UNKNOWN.into())
}

pub fn display_server() -> String {
    match env::var("XDG_SESSION_TYPE") {
        Ok(s) => match s.as_str() {
            "x11" => "X11".into(),
            "wayland" => "Wayland".into(),
            other => other.into(),
        },
        Err(_) => UNKNOWN.into(),
    }
}
