use super::UNKNOWN;
use std::fs::read_to_string;

pub fn host() -> String {
    read_to_string("/etc/hostname")
        .map(|s| s.trim().to_string())
        .unwrap_or(UNKNOWN.into())
}
