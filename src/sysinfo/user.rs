use super::UNKNOWN;
use std::env;

pub fn user() -> String {
    env::var("USER").unwrap_or(UNKNOWN.into())
}
