use super::UNKNOWN;
use std::fs::read_to_string;

pub fn cpu_name() -> String {
    read_to_string("/proc/cpuinfo")
        .ok()
        .and_then(|s| {
            s.split("\n")
                .into_iter()
                .find(|line| line.starts_with("model name"))
                .map(|line| line.split(':').last().map(|s| s.trim().to_string()))
        })
        .flatten()
        .unwrap_or(UNKNOWN.into())
}
