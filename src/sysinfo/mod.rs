pub mod cpu;
pub mod desktop;
pub mod distro;
pub mod gpu;
pub mod host;
pub mod kernel;
pub mod motherboard;
pub mod ram;
pub mod user;

pub const UNKNOWN: &str = "Unknown";
