use std::fs::read_to_string;

use super::UNKNOWN;

pub fn kernel_version() -> String {
    read_to_string("/proc/version")
        .ok()
        .map(|data| {
            data.split_ascii_whitespace()
                .nth(2)
                .map(|s| s.trim().to_string())
        })
        .flatten()
        .unwrap_or(UNKNOWN.into())
        .into()
}
