use super::UNKNOWN;
use chrono::NaiveDate;
use std::fs::read_to_string;

pub fn motherboard_name() -> String {
    read_to_string("/sys/devices/virtual/dmi/id/board_name")
        .unwrap_or(UNKNOWN.into())
        .trim()
        .into()
}

pub fn bios_version() -> String {
    read_to_string("/sys/devices/virtual/dmi/id/bios_release")
        .map(|s| s.trim().to_string())
        .unwrap_or(UNKNOWN.into())
}

pub fn bios_date() -> String {
    read_to_string("/sys/devices/virtual/dmi/id/bios_date")
        .ok()
        .map(|d| NaiveDate::parse_from_str(d.trim(), "%m/%d/%Y").ok())
        .flatten()
        .map(|d| d.format("%-d %B %Y").to_string())
        .unwrap_or(UNKNOWN.into())
}
