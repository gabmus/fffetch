use std::{fmt::Display, fs::read_to_string};

#[derive(Debug, Clone, Copy)]
pub struct Memory {
    pub total_kb: usize,
    pub available_kb: usize,
}

impl Memory {
    fn value_from_meminfo(row: &str) -> usize {
        row.split_ascii_whitespace()
            .nth(1)
            .map(|s| s.trim().parse::<usize>().ok())
            .flatten()
            .unwrap_or(0)
    }

    pub fn get() -> Self {
        let mut total_kb = 0;
        let mut available_kb = 0;
        if let Ok(meminfo) = read_to_string("/proc/meminfo") {
            for row in meminfo.split("\n") {
                if row.starts_with("MemTotal:") {
                    total_kb = Self::value_from_meminfo(row);
                } else if row.starts_with("MemAvailable:") {
                    available_kb = Self::value_from_meminfo(row);
                }
                if total_kb != 0 && available_kb != 0 {
                    break;
                }
            }
        }
        Self {
            total_kb,
            available_kb,
        }
    }

    fn kb_to_gb(n: usize) -> f64 {
        f64::from(n as u32) / 1024.0 / 1024.0
    }

    pub fn total_gb(&self) -> f64 {
        Self::kb_to_gb(self.total_kb)
    }

    pub fn available_gb(&self) -> f64 {
        Self::kb_to_gb(self.available_kb)
    }
}

impl Display for Memory {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let total = self.total_gb();
        let avail = self.available_gb();
        f.write_str(&format!("{:.2} / {:.2} GB", avail, total))
    }
}

pub fn ram_amount_kb() -> usize {
    read_to_string("/proc/meminfo")
        .ok()
        .map(|s| {
            s.split("\n")
                .into_iter()
                .find(|row| row.starts_with("MemTotal:"))
                .map(|s| {
                    s.split_ascii_whitespace()
                        .nth(1)
                        .map(|s| s.trim().to_string().parse::<usize>().ok())
                })
        })
        .flatten()
        .flatten()
        .flatten()
        .unwrap_or(0)
}

pub fn ram_amount_gb() -> f64 {
    f64::from(ram_amount_kb() as u32) / 1024.0 / 1024.0
}
