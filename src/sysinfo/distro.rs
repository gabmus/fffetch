use super::UNKNOWN;
use std::fs::read_to_string;

pub fn distro() -> String {
    read_to_string("/etc/os-release")
        .ok()
        .map(|s| {
            s.split("\n")
                .into_iter()
                .find(|line| line.starts_with("PRETTY_NAME="))
                .map(|n| n.split("=").last().map(|s| s.replace('"', "")))
        })
        .flatten()
        .flatten()
        .unwrap_or(UNKNOWN.into())
        .into()
}
