use clap::Parser;

use crate::{
    args::Args,
    datamodel::DataModel,
    framer::{frame_rounded, frame_text, FRAME_HR},
    icons::Icon,
    pad::pad,
    sysinfo::{
        cpu::cpu_name,
        desktop::{desktop, display_server},
        distro::distro,
        gpu::gpu_names,
        host::host,
        kernel::kernel_version,
        motherboard::{bios_date, bios_version, motherboard_name},
        ram::ram_amount_gb,
        user::user,
    },
    termcolor::{color_grid, t_blue, t_green},
};

pub mod args;
pub mod datamodel;
pub mod file_utils;
pub mod framer;
pub mod icons;
pub mod pad;
pub mod strlen;
pub mod sysinfo;
pub mod termcolor;

const MAX_TITLE_LEN: usize = 7;

fn main() {
    let args = Args::parse();

    let distro = distro();
    let mut text = [
        format!(
            "{user}@{host}",
            user = t_blue(&user()),
            host = t_green(&host()),
        ),
        (if !args.no_frame { FRAME_HR } else { "" }).into(),
        DataModel::new(Some(Icon::for_distro(&distro)), "Distro".into(), distro)
            .to_color_string(MAX_TITLE_LEN),
        DataModel::new(Some(Icon::Tux), "Kernel".into(), kernel_version())
            .to_color_string(MAX_TITLE_LEN),
        DataModel::new(
            Some(Icon::Desktop),
            "DE".into(),
            format!("{} ({})", desktop(), display_server()),
        )
        .to_color_string(MAX_TITLE_LEN),
        (if !args.no_frame { FRAME_HR } else { "" }).into(),
        DataModel::new(Some(Icon::Cpu), "CPU".into(), cpu_name()).to_color_string(MAX_TITLE_LEN),
        DataModel::new(
            Some(Icon::Memory),
            "RAM".into(),
            format!("{:.2} GB", ram_amount_gb()),
        )
        .to_color_string(MAX_TITLE_LEN),
        DataModel::new(Some(Icon::Gpu), "GPU".into(), gpu_names()).to_color_string(MAX_TITLE_LEN),
        DataModel::new(Some(Icon::Motherboard), "Mobo".into(), motherboard_name())
            .to_color_string(MAX_TITLE_LEN),
        DataModel::new(
            Some(Icon::Firmware),
            "BIOS".into(),
            format!("{} ({})", bios_version(), bios_date()),
        )
        .to_color_string(MAX_TITLE_LEN),
    ]
    .join("\n");

    if args.colorgrid {
        text += &format!(
            "\n{}\n{}",
            if args.no_frame { "" } else { FRAME_HR },
            color_grid()
        );
    }

    if !args.no_frame {
        let frame = frame_rounded();
        println!("{}", frame_text(&pad(&text, 0, 1, 0, 1), &frame));
    } else {
        println!("{}", text);
    }
}
