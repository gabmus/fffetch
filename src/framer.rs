use crate::strlen::strlen;
use std::cmp::max;

pub struct Frame {
    /** north */
    pub n: String,
    /** north-east */
    pub ne: String,
    /** east */
    pub e: String,
    /** south-east */
    pub se: String,
    /** south */
    pub s: String,
    /** south-west */
    pub sw: String,
    /** west */
    pub w: String,
    /** north-west */
    pub nw: String,

    pub wr: String,
    pub wl: String,
    pub er: String,
    pub el: String,
    pub su: String,
    pub sd: String,
    pub nu: String,
    pub nd: String,
}

pub fn frame_rounded() -> Frame {
    Frame {
        n: "─".into(),
        ne: "╮".into(),
        e: "│".into(),
        se: "╯".into(),
        s: "─".into(),
        sw: "╰".into(),
        w: "│".into(),
        nw: "╭".into(),
        wr: "├".into(),
        wl: "┤".into(),
        er: "├".into(),
        el: "┤".into(),
        su: "┴".into(),
        sd: "┬".into(),
        nu: "┴".into(),
        nd: "┬".into(),
    }
}

pub fn rightpad(text: &str, size: usize) -> String {
    format!(
        "{text}{padding}",
        text = text,
        padding = " ".repeat(max(0, size - strlen(text)))
    )
}

pub const FRAME_HR: &str = "\u{ffff}%FRAME_HR%\u{ffff}";

pub fn frame_text(text: &str, frame: &Frame) -> String {
    let lines: Vec<&str> = text.split('\n').collect();
    let max_line_size = lines.iter().map(|s| strlen(s)).max().unwrap_or(0);
    format!(
        "{top}\n{inner}\n{bottom}",
        top = format!(
            "{topleft}{size}{topright}",
            topleft = frame.nw,
            size = frame.n.repeat(max_line_size),
            topright = frame.ne
        ),
        inner = lines
            .into_iter()
            .map(|s| if s.trim() == FRAME_HR {
                format!(
                    "{l}{i}{r}",
                    l = frame.wr,
                    i = frame.n.repeat(max_line_size),
                    r = frame.el
                )
            } else {
                format!(
                    "{l}{t}{r}",
                    l = frame.w,
                    t = rightpad(s, max_line_size),
                    r = frame.e
                )
            })
            .collect::<Vec<String>>()
            .join("\n"),
        bottom = format!(
            "{botleft}{size}{botright}",
            botleft = frame.sw,
            size = frame.s.repeat(max_line_size),
            botright = frame.se
        ),
    )
}

mod tests {
    #[test]
    fn can_draw_frames() {
        use super::{frame_text, Frame, FRAME_HR};
        assert_eq!(
            frame_text(
                &format!(
                    "hello world\n{}\nLorem ipsum dolor sit\namet\nconsectetur",
                    FRAME_HR
                ),
                &Frame {
                    n: "n".into(),
                    ne: "-".into(),
                    e: "e".into(),
                    se: "/".into(),
                    s: "s".into(),
                    sw: "-".into(),
                    w: "w".into(),
                    nw: "/".into(),
                    wr: "w".into(),
                    wl: " ".into(),
                    er: " ".into(),
                    el: "e".into(),
                    su: " ".into(),
                    sd: " ".into(),
                    nu: " ".into(),
                    nd: " ".into(),
                }
            ),
            concat!(
                "/nnnnnnnnnnnnnnnnnnnnn-\n",
                "whello world          e\n",
                "wnnnnnnnnnnnnnnnnnnnnne\n",
                "wLorem ipsum dolor site\n",
                "wamet                 e\n",
                "wconsectetur          e\n",
                "-sssssssssssssssssssss/",
            )
        )
    }
}
