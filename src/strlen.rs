use crate::termcolor::strip_color;

pub fn strlen(s: &str) -> usize {
    strip_color(&s).chars().count()
}
