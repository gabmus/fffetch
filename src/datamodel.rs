use crate::{
    icons::Icon,
    termcolor::{t_blue, t_bold},
};
use std::cmp::max;

#[derive(Debug, Clone)]
pub struct DataModel {
    icon: Option<Icon>,
    title: String,
    body: String,
}

impl DataModel {
    pub fn new(icon: Option<Icon>, title: String, body: String) -> Self {
        Self { icon, title, body }
    }

    pub fn to_color_string(&self, title_size: usize) -> String {
        format!(
            "{i}{title}:{spaces}{t}",
            i = t_blue(
                &(if let Some(i) = self.icon {
                    i.to_string() + " "
                } else {
                    "".into()
                })
            ),
            title = t_bold(&self.title),
            spaces = " ".repeat(max(title_size - self.title.len(), 1)),
            t = self.body
        )
    }
}
