use std::{fs::File, io::BufReader, path::Path};

pub fn get_reader(path_s: &str) -> Option<BufReader<File>> {
    let path = Path::new(&path_s);
    match File::open(path) {
        Err(_) => None,
        Ok(fd) => Some(BufReader::new(fd)),
    }
}
