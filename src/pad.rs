pub fn pad(text: &str, n: usize, e: usize, s: usize, w: usize) -> String {
    format!(
        "{top}{inner}{bottom}",
        top = "\n".repeat(n),
        inner = text
            .split("\n")
            .into_iter()
            .map(|line| format!(
                "{l}{txt}{r}",
                l = " ".repeat(w),
                txt = line,
                r = " ".repeat(e)
            ))
            .collect::<Vec<String>>()
            .join("\n"),
        bottom = "\n".repeat(s)
    )
}

mod tests {
    #[test]
    fn can_pad() {
        use super::pad;
        assert_eq!(
            pad("foo\nbar\nbar baz", 1, 2, 3, 4),
            "\n    foo  \n    bar  \n    bar baz  \n\n\n"
        )
    }
}
